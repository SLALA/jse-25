package ru.t1.strelcov.tm.bootstrap;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import ru.t1.strelcov.tm.api.IPropertyService;
import ru.t1.strelcov.tm.api.repository.*;
import ru.t1.strelcov.tm.api.service.*;
import ru.t1.strelcov.tm.command.AbstractCommand;
import ru.t1.strelcov.tm.enumerated.Role;
import ru.t1.strelcov.tm.repository.*;
import ru.t1.strelcov.tm.service.*;
import ru.t1.strelcov.tm.util.TerminalUtil;

import java.lang.reflect.Modifier;
import java.util.Comparator;
import java.util.Set;

public final class Bootstrap implements ServiceLocator {

    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();

    @NotNull
    private final ICommandService commandService = new CommandService(commandRepository);

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    @NotNull
    private final ILoggerService loggerService = new LoggerService();

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @NotNull
    private final IUserService userService = new UserService(userRepository, propertyService);

    @NotNull
    private final IAuthService authService = new AuthService(userService, propertyService);

    private void initCommands() {
        @NotNull final Reflections reflections = new Reflections(AbstractCommand.class.getPackage().getName());
        @NotNull final Set<Class<? extends AbstractCommand>> classes = reflections.getSubTypesOf(AbstractCommand.class);
        classes.stream()
                .filter((clazz) -> !Modifier.isAbstract(clazz.getModifiers()))
                .sorted(Comparator.comparing(Class::getName))
                .forEach((clazz) -> {
                    try {
                        register(clazz.newInstance());
                    } catch (InstantiationException | IllegalAccessException e) {
                        loggerService.errors(e);
                    }
                });
    }

    public void register(@NotNull final AbstractCommand command) {
        command.setServiceLocator(this);
        commandService.add(command);
    }

    private void initUsers() {
        userService.add("test", "test", Role.USER);
        userService.add("admin", "admin", Role.ADMIN);
    }

    private void initData() {
        @Nullable String userId = userService.findByLogin("test").getId();
        @Nullable String projectId = projectService.add(userId, "p1", "p1").getId();
        projectService.add(userId, "p2", "p2");
        taskService.add(userId, "t1", "t1").setProjectId(projectId);
        taskService.add(userId, "t2", "t2").setProjectId(projectId);
        userId = userService.findByLogin("admin").getId();
        projectId = projectService.add(userId, "pa1", "pa1").getId();
        projectService.add(userId, "pa2", "pa2");
        taskService.add(userId, "a1", "a1").setProjectId(projectId);
        taskService.add(userId, "a2", "a2").setProjectId(projectId);
    }

    public void run(@Nullable String... args) {
        initCommands();
        initUsers();
        initData();
        displayWelcome();

        try {
            if (parseArgs(args))
                System.exit(0);
        } catch (Exception e) {
            loggerService.errors(e);
            System.exit(0);
        }
        while (true) {
            try {
                System.out.println("ENTER COMMAND:");
                parseCommand(TerminalUtil.nextLine());
                System.out.println("[OK]");
            } catch (Exception e) {
                loggerService.errors(e);
                System.err.println("[FAIL]");
            } finally {
                System.out.println();
            }
        }
    }

    public void displayWelcome() {
        loggerService.info("** WELCOME TO TASK MANAGER **");
    }

    public boolean parseArgs(@Nullable final String[] args) {
        if (args == null || args.length < 1) return false;
        parseArg(args[0]);
        return true;
    }

    public void parseArg(@Nullable final String arg) {
        if (arg == null) return;
        loggerService.commands(arg);
        @NotNull final AbstractCommand abstractCommand = commandService.getCommandByArg(arg);
        abstractCommand.execute();
    }

    public void parseCommand(@Nullable final String command) {
        if (command == null) return;
        loggerService.commands(command);
        @NotNull final AbstractCommand abstractCommand = commandService.getCommandByName(command);
        authService.checkRoles(abstractCommand.roles());
        abstractCommand.execute();
    }

    @NotNull
    @Override
    public ITaskService getTaskService() {
        return taskService;
    }

    @NotNull
    @Override
    public IProjectService getProjectService() {
        return projectService;
    }

    @NotNull
    @Override
    public ICommandService getCommandService() {
        return commandService;
    }

    @NotNull
    @Override
    public IProjectTaskService getProjectTaskService() {
        return projectTaskService;
    }

    @NotNull
    @Override
    public IUserService getUserService() {
        return userService;
    }

    @NotNull
    @Override
    public IAuthService getAuthService() {
        return authService;
    }

    @NotNull
    @Override
    public IPropertyService getPropertyService() {
        return propertyService;
    }
}
