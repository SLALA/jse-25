package ru.t1.strelcov.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.strelcov.tm.api.IPropertyService;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertyService implements IPropertyService {

    @NotNull
    private static final String CONFIG_FILE = "application.properties";

    @NotNull
    private static final String PASSWORD_SECRET_KEY = "secret";

    @NotNull
    private static final String PASSWORD_ITERATION_KEY = "iteration";

    @NotNull
    private static final String EMAIL_KEY = "email";

    @NotNull
    private static final String VERSION_KEY = "version";

    @NotNull
    private static final String NAME_KEY = "name";

    @NotNull
    private static final String PASSWORD_SECRET_DEFAULT = "";

    @NotNull
    private static final String PASSWORD_ITERATION_DEFAULT = "1";

    @NotNull
    private static final String EMAIL_DEFAULT = "";

    @NotNull
    private static final String VERSION_DEFAULT = "";

    @NotNull
    private static final String NAME_DEFAULT = "";

    @NotNull
    private final Properties properties = new Properties();

    @SneakyThrows
    public PropertyService() {
        final InputStream inputStream = ClassLoader.getSystemResourceAsStream(CONFIG_FILE);
        if (inputStream == null) return;
        properties.load(inputStream);
        inputStream.close();
    }

    @NotNull
    private String getPropertyValue(@NotNull String key, @NotNull String defaultValue) {
        if (System.getenv().containsKey(key)) return System.getenv(key);
        if (System.getProperties().containsKey(key)) return System.getProperty(key);
        return properties.getProperty(key, defaultValue);
    }

    @NotNull
    private Integer getPropertyIntValue(@NotNull String key, @NotNull String defaultValue) {
        @NotNull final String value;
        if (System.getenv().containsKey(key)) value = System.getenv(key);
        else if (System.getProperties().containsKey(key)) value = System.getProperty(key);
        else value = properties.getProperty(key, defaultValue);
        return Integer.parseInt(value);
    }

    @NotNull
    @Override
    public String getPasswordSecret() {
        return getPropertyValue(PASSWORD_SECRET_KEY, PASSWORD_SECRET_DEFAULT);
    }

    @NotNull
    @Override
    public Integer getPasswordIteration() {
        return getPropertyIntValue(PASSWORD_ITERATION_KEY, PASSWORD_ITERATION_DEFAULT);
    }

    @NotNull
    @Override
    public String getName() {
        return getPropertyValue(NAME_KEY, NAME_DEFAULT);
    }

    @NotNull
    @Override
    public String getVersion() {
        return getPropertyValue(VERSION_KEY, VERSION_DEFAULT);
    }

    @NotNull
    @Override
    public String getEmail() {
        return getPropertyValue(EMAIL_KEY, EMAIL_DEFAULT);
    }

}