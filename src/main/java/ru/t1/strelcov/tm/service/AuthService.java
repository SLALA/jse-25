package ru.t1.strelcov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.strelcov.tm.api.IPropertyService;
import ru.t1.strelcov.tm.api.service.IAuthService;
import ru.t1.strelcov.tm.api.service.IUserService;
import ru.t1.strelcov.tm.enumerated.Role;
import ru.t1.strelcov.tm.exception.empty.*;
import ru.t1.strelcov.tm.exception.entity.*;
import ru.t1.strelcov.tm.model.User;
import ru.t1.strelcov.tm.util.HashUtil;

import java.util.Optional;

public final class AuthService implements IAuthService {

    @NotNull
    private final IUserService userService;

    @NotNull
    private final IPropertyService propertyService;

    @Nullable
    private String userId;

    public AuthService(@NotNull final IUserService userService, @NotNull IPropertyService propertyService) {
        this.userService = userService;
        this.propertyService = propertyService;
    }

    @NotNull
    @Override
    public String getUserId() {
        return Optional.ofNullable(userId).orElseThrow(AccessDeniedException::new);
    }

    @NotNull
    @Override
    public User getUser() {
        Optional.ofNullable(userId).orElseThrow(AccessDeniedException::new);
        return userService.findById(userId);
    }

    @Override
    public void login(@Nullable final String login, @Nullable final String password) {
        Optional.ofNullable(login).filter((i) -> !i.isEmpty()).orElseThrow(EmptyLoginException::new);
        Optional.ofNullable(password).filter((i) -> !i.isEmpty()).orElseThrow(EmptyPasswordException::new);
        @NotNull final User user = userService.findByLogin(login);
        @NotNull final String passwordHash = HashUtil.salt(propertyService.getPasswordSecret(), propertyService.getPasswordIteration(), password);
        if (!user.getPasswordHash().equals(passwordHash)) throw new IncorrectPasswordException();
        if (user.getLocked()) throw new UserLockedException();
        userId = user.getId();
    }

    @Override
    public void logout() {
        Optional.ofNullable(userId).orElseThrow(AccessDeniedException::new);
        userId = null;
    }

    @Override
    public void checkRoles(@Nullable final Role... roles) {
        if (roles == null || roles.length == 0) return;
        @NotNull final Role userRole = Optional.of(getUser()).map(User::getRole).orElseThrow(AccessDeniedException::new);
        for (final Role role : roles)
            if (userRole.equals(role)) return;
        throw new AccessDeniedException();
    }

}
